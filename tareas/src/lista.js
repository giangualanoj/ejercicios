import { useState, useEffect } from "react";
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';

const Lista = () => {
  const [users, setUsers] = useState([])
  const [selectedProduct, setSelectedProduct] = useState(null);

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/users")
      .then((r) => r.json())
      .then((data) => {
        setUsers(data)
        console.log(data)
      })
  }, [])

  const onRowSelect = (event) => {
    console.log(`Seleccionaste la fila ${event.data.name} que tiene el id ${event.data.id}`)
    window.location.href = `/detallesDeProducto/${event.data.id}`
  }

  return (
    <div>
      <h1 className="text-center">Tabla de Usuarios</h1>
      <DataTable value={users} stripedRows paginator rows={5} rowsPerPageOptions={[5, 10, 25, 50]} selectionMode="single" selection={selectedProduct}
        onSelectionChange={(e) => setSelectedProduct(e.value)} tableStyle={{ minWidth: '50rem' }} onRowSelect={ (event) => { onRowSelect(event) }}>
        <Column field="id" header="ID" sortable></Column>
        <Column field="name" header="Name" sortable></Column>
        <Column field="phone" header="Phone" sortable></Column>
        <Column field="website" header="Website" sortable></Column>
      </DataTable>
    </div>
  )
}

export default Lista

/* ejercicio 1 */