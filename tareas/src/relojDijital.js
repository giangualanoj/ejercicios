import React, { useState, useEffect } from "react";

export default function RelojDijital() {
    const [horaActual, setHoraActual] = useState(new Date().toLocaleTimeString());

    useEffect(() => {
        const intervalo = setInterval(() => {
            const hora = new Date().toLocaleTimeString();
            setHoraActual(hora);
        }, 1000);

        return () => clearInterval(intervalo);
    }, []);


    return (
        <div>
            <h1>{horaActual}</h1>
        </div>
    )
}

/* ejercicio 5 */