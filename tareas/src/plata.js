import React, { useState } from 'react';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
  


export default function Plata() {
    const [dolar, setDolar] = useState('');
    const [cambio, setCambio] = useState(120);

    const peso = () => {
        setCambio(parseFloat(dolar) * cambio);
    }


    return (
        <div className='mb-3'>
            <InputText className='form-control' placeholder="Dolar a Pesos" type='text' value={dolar}
                onChange={(e) => setDolar(e.target.value)} />
            <Button label="Submit" icon="pi pi-check" onClick={peso}></Button>
            {cambio && <p>El cambio en pesos es: {cambio}</p>}
        </div>
    );
}
/* ejercicio 2 */