import React, { useState } from 'react';
import { InputText } from "primereact/inputtext";
import { Button } from 'primereact/button';
import { Checkbox } from "primereact/checkbox";

function TodoList() {
    const [tareas, setTareas] = useState([]);
    const [nuevaTarea, setNuevaTarea] = useState('');

    const agregar = () => {
        if (nuevaTarea.trim() !== '') {
            setTareas([...tareas, { tarea: nuevaTarea, checked: false }]);
            setNuevaTarea('');
        }
    };

    const eliminar = (index) => {
        const updatedtareas = tareas.filter((_, i) => i !== index);
        setTareas(updatedtareas);
    };

    const toggleTarea = (index) => {
        const updatedtareas = tareas.map((tarea, i) => {
            if (i === index) {
                return {
                    ...tarea,
                    checked: !tarea.checked
                };
            }
            return tarea;
        });
        setTareas(updatedtareas);
    };

    return ( 
        <div className='flex flex-column align-items-center gap-3'>
            <h1>TodoList</h1>
            <div className='flex align-items-center'>
                <InputText
                    type="text"
                    value={nuevaTarea}
                    onChange={(e) => setNuevaTarea(e.target.value)}
                    placeholder="Ingresar tarea"
                />
                <Button className='mx-2' icon="pi pi-check" rounded text raised aria-label="Add" onClick={agregar} />
            </div>
            
            <div className='flex align-items-center'>
            <ul>
                {tareas.map((tarea, index) => (
                    <li key={index} className='flex align-items-center'>
                        <Checkbox onChange={() => toggleTarea(index)} checked={tarea.checked} />
                        <span className={tarea.checked ? 'tarea-checked' : ''}>{tarea.tarea}</span>
                        <Button className='mx-2' icon="pi pi-times" rounded text raised severity="danger" aria-label="Cancel" onClick={() => eliminar(index)} />
                    </li>
                ))}
            </ul>
            </div>

        </div>
    );
}

export default TodoList;


/* ejercicio 3 */